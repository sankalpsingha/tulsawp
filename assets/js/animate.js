
/*---- Scroll To Elements ---- */

$(document).ready(function () {

    $('.header-animate-btn').on('click', function () {

        //this scroll withour animations in chrome
        $('p').get(0).scrollIntoView({
            block: "start",
            behavior: "smooth"
        });

    });


    function scrollToElement(element, parent) {
        $(parent)[0].scrollIntoView(true);
        $(parent).animate({
            scrollTop: $(parent).scrollTop() + $(element).offset().top - $(parent).offset().top
        }, {
                duration: 'slow',
                easing: 'swing'
            });
    }

    //call animated scroll script on click of 2nd button
    $('.header-animate-btn').on('click', function () {
        var paretq = $('header');
        var elemq = $('.about-section');
        scrollToElement(elemq, paretq);
    })
})