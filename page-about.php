<?php get_header(); ?>

  <!-- Header -->
  <header class="section-header">
    <div class="container">
      <div class="header-textbox">
        <h1 class="about-us">About Us</h1>
        <!-- <h2 class="live-blessed">#Live Blessed</h2> -->
      </div>
    </div>
    <div class="header-background"></div>
  </header>

  <!-- About -->
  <section class="section-about">
    <div class="container">
      <div class="tulsa-logo-container">
      <img src="<?php echo get_template_directory_uri() . '/assets/images/Tulsa_logo.png'?>" alt="tulsa logo" class="tulsa-logo">
      </div>
      <div class="about-content p-30">
        <h3 class="about-title-header text-center  mt-60">About Us</h3>
        <p class="about-paragraph">Tulsa began business in the 1980’s with a very humble start in Siliguri. Our Chairman, Mr. Suresh Kumar Agarwal, had a simple vision, “To make beautiful spaces that were cost-effective”. With that vision in mind, he set about building the first specialist sanitation store in the city that offered ready- made tiles in a marble driven market. It was revolutionary at the time and with the ease that Tiles offered over natural stones, he created an enterprise that dominated the market for over three decades with a store, that remains to this day, one of the best dealers for hardware in the city.</p>
        
        
        <p class="about-paragraph"> We had our start providing customers with everything they need to make beautiful homes and in 2001 we made our first venture into real-estate with Shanti Towers, a commercial property over 1200 sq ft and housed major brands the likes of Citi Style and Akash Institutes.</p>
        
        <p class="about-paragraph"> Since then we have built many spaces and we are proud to be affiliated with a large roster of brands including Aditya Birla Group, Sanjeev Kapoor’s Yellow Chilli and many more. We have successfully built over 800000 sq ft of space so far and we our mission is to help over 50000 families find their ideal homes by the year 2060!</p>

      </div>
    </div>
  </section>

  <section class="section-team">
    <div class="container">
      <div class="section-wrapper p-30">
          <h3 class="about-title-header">Our Team</h3>
          <div class="team-img-container">
              <img src="<?php echo get_template_directory_uri() . '/assets/images/office team.jpg' ?>" alt="team-img" class="team-img img-fluid">
          </div>
      </div>
    </div>
  </section>

  <section class="section-core-members ">
    <div class="container">
    <h3 class="about-title-header text-center">CORE MEMBERS</h3>
      <div class="member-section">
        <div class="image-part">

        </div>

        <div class="text-part">
          <h2 class="text-part__title">Suresh Kumar</h2>
          <p>A man with a humble beginnings, he marked his way from doing petty errands at a tea garden for an everyday wage to setting up the ﬁrst sanitation trading store in Siliguri city, to becoming a conglomerate in multiple sectors including hospitality and real estate. He believes there’s no shortcut to hard work and persistency, and success comes only to the bold</p>
          
        </div>
      </div>


      <div class="member-section-alternate">

          <div class="text-part">
            <h2 class="text-part__title">Prateek Garg</h2>
            <p> Pratik joined the group at the age of 20 completing his Masters in HR, spanning his efforts across multiple sectors. Starting with retail sanitation trading at Garg Enterprises, he applied his efforts on multiple projects including hotels and commercial realty. His accomplishments include setting up multiple high value spaces excluding his more philanthropic activities.</p>
          </div>
          <div class="image-part">
          </div>

      </div>


    </div>

    </div>
  </section>


<?php get_footer(); ?>