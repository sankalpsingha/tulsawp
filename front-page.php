<?php get_header(); ?>

  <header>
    <div class="container header-section">
      <div class="header-text-wrap">
        <h1>TULSA</h1>
        <h2>#LiveBlessed</h2>
        <p>We aim to deliver a living experience as pure as it could be. Finding balance while staying rooted to the energies of mother nature, we want to build spaces complete in all aspects so our clients can live in wholesome and healthy environments that offer more than just a simple roof over their heads</p>
        <a href="#" class="read-more header-animate-btn">Read More</a>
      </div>
    </div>
  </header>

  <section class="about-section p-40">
    <h2 class="title-header">Live Blessed. Always.</h2>
    <div class="container">
      <div class="row mt-30 responsive-about-section">
        <div class="col-6 ">
          <div class="section-about-text">
            <p class="about-text-paragraph-1">We follow a philosophy of making the space work for its occupants and not the other way around because we understand what it means to invest in a space to call your own. It is more than just a space, it is a life that you have planned for the years to come and we want to help make that life as ideal as possible by designing structures with people at the heart of it. </p>

            <p class="about-text-paragraph-2">We create spaces that are completely inclusive with homes that are meant for families and individuals alike. An investment made for a life and a future which we hope will allow you to Live Blessed Always</p>

            <a href="#" class="section-btn">Learn More</a>
          </div>
        </div>
        <div class="col-6 live-blessed-graphic">
          <img src="<?php echo get_template_directory_uri() . '/assets/images/TulsaArtwork_Transparent.png' ?>" alt="Tulsa Artwork" class="tulsa-artwork img-fluid">
        </div>
      </div>
    </div>
  </section>

  <section class="brand-section p-40">
    <h2 class="title-header">Brands Love Us</h2>
    <div class="container">
      <div class="row mt-30">
        <div class="col"><img class="brand-logo img-fluid" src="<?php echo get_template_directory_uri() . '/assets/images/IREX_Logo_Grey-H.png' ?>"" alt="Logo"></div>
        <div class="col"><img class="brand-logo img-fluid" src="<?php echo get_template_directory_uri() . '/assets/images/IREX_Logo_Grey-H.png' ?>"" alt="Logo"></div>
        <div class="col"><img class="brand-logo img-fluid" src="<?php echo get_template_directory_uri() . '/assets/images/IREX_Logo_Grey-H.png' ?>"" alt="Logo"></div>
      </div>

      <div class="row mt-30">
      <div class="col"><img class="brand-logo img-fluid" src="<?php echo get_template_directory_uri() . '/assets/images/IREX_Logo_Grey-H.png' ?>"" alt="Logo"></div>
        <div class="col"><img class="brand-logo img-fluid" src="<?php echo get_template_directory_uri() . '/assets/images/IREX_Logo_Grey-H.png' ?>"" alt="Logo"></div>
        <div class="col"><img class="brand-logo img-fluid" src="<?php echo get_template_directory_uri() . '/assets/images/IREX_Logo_Grey-H.png' ?>"" alt="Logo"></div>
      </div>
    </div>
  </section>


  <section class="testimonial-section">
    <div class="testimonial-image1 img-fluid"></div>
    <div class="testimonial-image2 img-fluid"></div>
    <div class="testimonial-image3 img-fluid"></div>

    <div class="testimonial-box">
      <div class="testimonial-box-border">
        <div class="slider-content-slide__inner__content">
          <blockquote class="blockquote-text">
            We would love to work with Tulsa, It has been amazing !!
          </blockquote>
          <div class="clients-info">
            <span class="clients-info__name">Mr. Narpat Singh Surana</span>
            <span class="clients-info__designation">The Address</span>
          </div>
        </div>
      </div>
    </div>
  </section>


  <section class="blogs-section p-40">
    <h2 class="title-header blog-header">Our latest posts</h2>
    <div class="container">
      <div class="row mt-30 responsive-blog-row">
        <div class="col blogs-container">
          <div class="dream-house-img-container">
            <img class="dream-house-img img-fluid" src="<?php echo get_template_directory_uri() . '/assets/images/Blog.jpg'?>" alt="Dream house">
          </div>
          <div class="content-container-box">
            <h4 class="blog-title font-italic">The house of your Dream</h4>
            <p class="blog-para">This is such a cool house that, you would like Lorem, ipsum dolor sit amet
              consectetur adipisicing
            </p>
            <a href="#" class="blog-read-more font-italic">Read More +</a>
          </div>
        </div>
        <div class="col blogs-container">
            <div class="dream-house-img-container">
              <img class="dream-house-img img-fluid" src="<?php echo get_template_directory_uri() . '/assets/images/Blog.jpg'?>" alt="Dream house">
            </div>
            <div class="content-container-box">
              <h4 class="blog-title font-italic">The house of your Dream</h3>
                <p class="blog-para">This is such a cool house that, you would like Lorem, ipsum dolor sit amet
                  consectetur adipisicing
                </p>
                <a href="#" class="blog-read-more font-italic">Read More +</a>
            </div>
        </div>
        <div class="col blogs-container responsive-display-blogs-container">
            <div class="dream-house-img-container">
              <img class="dream-house-img img-fluid" src="<?php echo get_template_directory_uri() . '/assets/images/Blog.jpg'?>" alt="Dream house">
            </div>
            <div class="content-container-box">
              <h4 class="blog-title font-italic">The house of your Dream</h3>
                <p class="blog-para">This is such a cool house that, you would like Lorem, ipsum dolor sit amet
                  consectetur adipisicing
                </p>
                <a href="#" class="blog-read-more font-italic">Read More +</a>
            </div>
        </div>
    </div>
  </section>

  <section class="subscribe-section p-40">
      <div class="container">
        <h4 class="subscribe-heading font-italic">Stay updated about our Projects</h4>
        <div class="subscribe-form">
          <input type="text" class="subscribe-input" placeholder="Enter email here">
          <button class="subscribe-btn">Subscribe</button>
        </div>
      </div>
  </section>

  <section class="contact-section">
    <div class="container">
      <div class="row responsive-contact-row">
        <div class="col contact-box">
          <div class="box">
            <h2 class="contact-box-header">MAIL</h2>
            <p class="contact-box-content">xyz@gmail.com</p>
          </div>
        </div>
        <div class="col contact-box">
          <div class="box">
            <h2 class="contact-box-header">CALL</h2>
            <p class="contact-box-content">+91 81234567890</p>
          </div>
        </div>
        <div class="col contact-box">
          <div class="box">
            <h2 class="contact-box-header">SOCIAL</h2>
            <p class="contact-box-content">xyz@gmail.com</p>
          </div>
        </div>
      </div>
    </div>
  </section>

  <?php get_footer(); ?>