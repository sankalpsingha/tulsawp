<?php 

function tulsa_after_setup_theme() {
    add_theme_support('title-tag');

    // Adding the custom Logo Support 
    $defaults = array(
        'height'      => 88,
        'width'       => 265,
        'flex-height' => true,
        'flex-width'  => true,
        'header-text' => array( 'site-title', 'site-description', 'main-logo' ),
        );
    add_theme_support( 'custom-logo' , $defaults);



    register_nav_menu(
        'primary', __('Primary Menu', 'tulsa')
    );
}