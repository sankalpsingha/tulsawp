<?php 

function tulsa_enqueue() {
    wp_register_style('tulsa_bootstrap', get_template_directory_uri() . '/assets/css/bootstrap.min.css');
    wp_register_style('tulsa_style', get_template_directory_uri() . '/assets/css/style.css');

    wp_enqueue_style('tulsa_bootstrap');
    wp_enqueue_style('tulsa_style');

    wp_register_script('tulsa_popper', get_template_directory_uri() . '/assets/js/popper.min.js', array('jquery'), false, true);
    wp_register_script('tulsa_bootstrap', get_template_directory_uri() . '/assets/js/bootstrap.min.js', array('jquery'), false, true);

    wp_enqueue_script('tulsa_popper');
    wp_enqueue_script('tulsa_bootstrap');

    function jquery_mumbo_jumbo()
    {
        wp_dequeue_script('jquery');
        wp_dequeue_script('jquery-core');
        wp_dequeue_script('jquery-migrate');
        wp_enqueue_script('jquery', false, array(), false, true);
        wp_enqueue_script('jquery-core', false, array(), false, true);
        wp_enqueue_script('jquery-migrate', false, array(), false, true);
    }
    add_action('wp_enqueue_scripts', 'jquery_mumbo_jumbo');
}   