<?php

// Setup

require_once get_template_directory() . '/bootstrap-navwalker.php';

// Includes

include( get_template_directory() . '/includes/enqueue.php');
include( get_template_directory() . '/includes/after_setup.php');
// Hooks 

add_filter('wp_enqueue_scripts', 'tulsa_enqueue');
add_filter('after_setup_theme', 'tulsa_after_setup_theme');

// Filters




// Shortcodes

