<footer class="footer-section skyline-img">
    <div class="container footer-wrapper">
      <div class="footer-container">
        <ul class="footer-links">
          <li><a class="foot-links" href="#">Disclaimer</a></li>
          <li><a class="foot-links" href="#">Privacy Policy</a></li>
          <li><a class="foot-links" href="#">Carrier</a></li>
        </ul>
        <div class="copyright-container">
          <h4 class="copyright">Cooked With love @ SuperPress</h4>
        </div>
      </div>
    </div>
  </footer>



  <?php wp_footer(); ?>
  

</body>

</html>